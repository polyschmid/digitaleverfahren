import ij.*;
import ij.plugin.filter.PlugInFilter;
import ij.process.*;
import java.awt.*;
/*
   Histogramm_ bestimmt das Histogramm
	 des ausgew�hlten Bildes und ist nur
	 auf 8 BitGraustufenbilder anwendbar.
*/
public class Histogramm_ implements PlugInFilter {

 String Bildname;
                        // �berpr�fung der Anwendbarkeit
 public int setup(String arg, ImagePlus imp) {
  if (arg.equals("about")) {showAbout(); return DONE;}
                        // Name des ausgew�hlten Bildes lesen.
  Bildname = imp.getShortTitle();
  return DOES_8G;
 }
                        // Die Methode run berechnet
                        // das Histogramm
 public void run(ImageProcessor ip) {
                        // Stufenanzahl des Histogramms
  int anzahl = 256;
                        // Histogrammfeld
  int[] histogramm = new int[anzahl];
  for (int i=0; i<anzahl; i++) histogramm[i]=0;

                        // Angaben zum Bildbereich (ROI) laden.
  Rectangle r = ip.getRoi();
                        // Schleifen ueber alle Bildpunkte des ROI
  for (int y=r.y; y<(r.y+r.height); y++) {
   for (int x=r.x; x<(r.x+r.width); x++) {
                        // Bildpunkt als Integer laden.
    int wert = ip.getPixel(x,y);
                        // Histogrammfeld aktualisieren
    histogramm[wert]++;
   }
  }
  int hmax=0;
                        // Maximum im Histogrammfeld ermitteln
  for (int i=0; i<anzahl; i++) if(hmax< histogramm[i]) hmax = histogramm[i];
                        // Histogrammfeld zeichnen
  zeichneHistogramm(anzahl, histogramm, hmax);
 }
                        // Die Methode zeichneHistogramm
                        // zeichnet das Histogramm
 private void zeichneHistogramm(int anzahl, int[] histogramm, int hmax) {
  int w = anzahl, h = anzahl;
	ImageProcessor ip2 = new ColorProcessor(w, h);
                        // Hintergrundfarbe w�hlen
  ip2.setColor(0xe3e3e3);
                        // Ziechenfl�che l�schen
  ip2.fill();
                        // Zeichenfarbe w�hlen
  ip2.setColor(0xffaeae);
                        // Histogramm zeichnen
  for (int x = 0; x < anzahl; x++) {
   int ywert = (hmax - histogramm[x]) * anzahl / hmax ;
   ip2.drawLine(x, anzahl , x, ywert);
  }
                        // Histogramm darstellen
	new ImagePlus("Histogramm zu "+Bildname, ip2).show();
//		new ImagePlus("Summenh�ufigkeit zu "+Bildname, ip).show();
 }
                        // Info zum Plugin
 void showAbout() {
  IJ.showMessage("Hilfe zu Histogramm_...",
   "Histogramm_ bestimmt das Histogramm \n" +
   "des ausgew�hlten Bildes und ist nur \n" +
   "auf 8 Bit Graustufenbilder anwendbar."
  );
 }
}

