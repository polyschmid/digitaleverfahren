
import ij.*;            // Benoetigte Klassen werden importiert
import ij.plugin.filter.PlugInFilter;
import ij.process.*;
import java.awt.*;

/*
   Mein_erstes_Plugin_ reduziert die
   Helligkeit des Bildes und ist nur
   auf 8 Bit Graustufenbilder anwendbar.
 */
// Um Bildpunkte lesen und schreiben zu
// koennen muss das Plugin PlugInFilter
// implementieren.
public class Mein_erstes_Plugin_ implements PlugInFilter {
    // Die Methode setup wird zu Beginn
    // aufgerufen, um die Anwendbarkeit
    // des Plugins auf das jeweils
    // ausgewaehlte Bild zu ueberpruefen.

    public int setup(String arg, ImagePlus imp) {
        // Bei Uebergabe von "about" wird der
        // Text in Methode showAbout ausgegeben
        // und die Methode run nicht aufgerufen.
        if (arg.equals("about")) {
            showAbout();
            return DONE;
        }
        // dieses Plugins ist nur auf 8 Bit
        // Graustufenbilder anwendbar, daher:
        return DOES_8G;
    }
    // Die Methode run verarbeitet
    // das ausgewaehlte Bild.

    public void run(ImageProcessor ip) {
        // Bildpixel kopieren
        byte[] pixels = (byte[]) ip.getPixels();
        // Bildbreite laden
        int width = ip.getWidth();
        // Angaben zum Bildbereich (ROI) laden.
        Rectangle r = ip.getRoi();
        int pixelindex;
        int wert;
        // Schleifen ueber alle Bildpunkte des ROI
        for (int y = r.y; y < (r.y + r.height); y++) {
            for (int x = r.x; x < (r.x + r.width); x++) {
                // Berechnung des Feldindex
                pixelindex = y * width + x;
                // Byte soll zwischen 0 ... 255 liegen.
                if (pixels[pixelindex] < 0) {
                    wert = 256 + (int) pixels[pixelindex];
                } else {
                    wert = (int) pixels[pixelindex];
                }
                // Helligkeit um 20 reduzieren
                wert -= 200;
                // Helligkeit auf 0 beschraenken
                if (wert < 0) {
                    wert = 0;
                }
                // Byte soll zwischen -128 ... 127 liegen.
                if (wert < 128) {
                    pixels[pixelindex] = (byte) wert;
                } else {
                    pixels[pixelindex] = (byte) (wert - 256);
                }
            }
        }
    }
    // Beschreibung der Funktion des Plugins.

    void showAbout() {
        IJ.showMessage("Hilfe zu Mein_erstes_Plugin_...",
                "Mein_erstes_Plugin_ reduziert die Hellig-  \n"
                + "keit des Bildes um den Wert 20 und ist     \n"
                + "nur auf 8 Bit Graustufenbilder anwendbar.");
    }
}
