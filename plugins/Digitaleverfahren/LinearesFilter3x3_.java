import ij.*;
import ij.plugin.filter.PlugInFilter;
import ij.process.*;
import java.awt.*;
/*
   LinearesFilter3x3_ filtert das ausgew�hlte
	 Bild mit einem 3 x 3 Filterkern. Es ist
	 nur auf 8 BitGraustufenbilder anwendbar.
*/
public class LinearesFilter3x3_ implements PlugInFilter {

 String Bildname;
                        // �berpr�fung der Anwendbarkeit
 public int setup(String arg, ImagePlus imp) {
  if (arg.equals("about")) {showAbout(); return DONE;}
                        // Name des ausgew�hlten Bildes lesen.
  Bildname = imp.getShortTitle();
  return DOES_8G;
 }
                        // Die Methode run berechnet
                        // das gefilterte Bild
 public void run(ImageProcessor ipin) {
  int w = ipin.getWidth(), h = ipin.getHeight();
                        // Ausgangsbildspeicher
	ImageProcessor ipout = new ByteProcessor(w, h);
                        // Deklaration des Filterkerns
  int[][] kernel = {{1, 1, 1},
                    {1, 1, 1},
                    {1, 1, 1}};
                        // Angaben zum Bildbereich (ROI) laden.
  Rectangle r = ipin.getRoi();
                        // Schleifen ueber alle Bildpunkte
   for (int y=0; y<h; y++) {
   for (int x=0; x<w; x++) {
                        // Eingangsbildpunkt laden.
    int wert = ipin.getPixel(x,y);
                        // nur filtern, wenn kein
                        // Randbereich.
    if(x>r.x && x<(r.x+r.width-1) &&
       y>r.y && y<(r.y+r.height-1)) {
     wert = 0;
                        // Schleifen ueber den Filterkern
     for (int iy=-1; iy<=1; iy++) {
      for (int ix=-1; ix<=1; ix++) {
       int yy=y+iy;
       int xx=x+ix;
       wert += kernel[1-ix][1-iy] * ipin.getPixel(xx,yy);
      }
     }
    }
                        // Normierung
    wert = wert/9;
                        // Ausgangsbildpunkt speichern
    ipout.putPixel(x,y,wert);
   }
  }
                        // Ausgangsbild zeigen
	new ImagePlus("gefiltertes "+Bildname, ipout).show();
 }
                       // Info zum Plugin
 void showAbout() {
  IJ.showMessage("Hilfe zu LinearesFilter3x3_...",
   "LinearesFilter3x3_ filtert das ausgew�hlte \n" +
	 "Bild mit einem 3 x 3 Filterkern. Es ist   \n" +
	 "nur auf 8 Bit Graustufenbilder anwendbar."
  );
 }
}

