
import ij.*;
import ij.plugin.filter.PlugInFilter;
import ij.process.*;
import java.awt.*;
import ij.gui.*;
import java.io.File;
import ij.ImagePlus;
import ij.io.FileSaver;

/*
   DVer_Aufgabe1 ver�ndert das Gamma des Bildes und ist nur
   auf 8 Bit Graustufenbilder anwendbar.
 */
// Um Bildpunkte lesen und schreiben zu
// koennen muss das Plugin PlugInFilter
// implementieren.
public class Aufgabe1 implements PlugInFilter {

    // Die Methode setup wird zu Beginn
    // aufgerufen, um die Anwendbarkeit
    // des Plugins auf das jeweils
    // ausgewaehlte Bild zu ueberpruefen.
    public int setup(String arg, ImagePlus imp) {
        // Bei Uebergabe von "about" wird der
        // Text in Methode showAbout ausgegeben
        // und die Methode run nicht aufgerufen.
        if (arg.equals("about")) {
            showAbout();
            return DONE;
        }
        // dieses Plugins ist nur auf 8 Bit
        // Graustufenbilder anwendbar, daher:
        return DOES_8G;
    }

    // Die Methode run verarbeitet
    // das ausgewaehlte Bild.
    /**
     * Ver�ndert das Gamma eines Bildes
     *
     * @param ip
     */
    public void run(ImageProcessor ip) {
        // Bildpixel kopieren
        byte[] pixels = (byte[]) ip.getPixels();
        // Bildbreite laden
        int width = ip.getWidth();
        // Angaben zum Bildbereich (ROI) laden.
        Rectangle r = ip.getRoi();
        int pixelindex;
        int wert;

        //Eingabewert interaktiv einlesen
        int Eingabewert = 1;
        GenericDialog gd = new GenericDialog("Was wird eingegeben?");
        gd.addNumericField("Eingabe 128...255:", Eingabewert, 0, 3, "");
        gd.showDialog();
        if (gd.wasCanceled()) {
            return;
        }
        Eingabewert = (int) gd.getNextNumber();

        // Schleifen ueber alle Bildpunkte des ROI
        for (int y = r.y; y < (r.y + r.height); y++) {
            for (int x = r.x; x < (r.x + r.width); x++) {
                wert = ip.getPixel(x, y);

                if (Eingabewert == 255 && wert > 128) {
                    wert = 255;
                }
                if (wert <= 128) {
                    wert = (Eingabewert / 128) * wert;
                }

                if (wert > 128) {

                    //
                    double b = (Eingabewert - ((255 - Eingabewert) / 127) * 128);
                    //
                    wert = (int) ((((255 - Eingabewert) / 127) * wert) + b);
                }

                if (wert < 0) {
                    wert = 0;
                }
                if (wert > 255) {
                    wert = 255;
                }

                //wert = wert + (Eingabewert/128);
                if (wert < 0) {
                    wert = 0;
                }

                ip.putPixel(x, y, wert);

            }
        }
        // Fragt, ob das Bild neu abgespeichert werden soll.
        GenericDialog gd2 = new GenericDialog("Speichern?");
        gd2.addNumericField("1: Ja; 2: Nein", Eingabewert, 0, 3, "");
        gd2.showDialog();
        if (gd2.wasCanceled()) {
            return;
        }
        Eingabewert = (int) gd2.getNextNumber();

        if (Eingabewert == 1) {
            ImagePlus imp = new ImagePlus();
            imp.setProcessor(ip);
            FileSaver fs = new FileSaver(imp);
            fs.saveAsJpeg();
        } else {
            return;
        }
    }
    // Beschreibung der Funktion des Plugins.

    void showAbout() {
        IJ.showMessage("Hilfe zu Mein_erstes_Plugin_...",
                "Mein_erstes_Plugin_ reduziert die Hellig-  \n"
                + "keit des Bildes um den Wert 20 und ist     \n"
                + "nur auf 8 Bit Graustufenbilder anwendbar.");
    }

}
