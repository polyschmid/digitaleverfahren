
import ij.plugin.filter.PlugInFilter;

import ij.*;
import ij.plugin.filter.PlugInFilter;
import ij.process.*;
import java.awt.*;
import ij.gui.*;
import java.io.File;
import ij.ImagePlus;
import ij.io.FileSaver;

/**
 *
 * @author Giulia und Hannes
 */
public class Aufgabe2 implements PlugInFilter {

    @Override
    public int setup(String string, ImagePlus ip) {
        if (string.equals("about")) {
            showAbout();
            return DONE;
        }

        return DOES_8G;
    }

    @Override
    public void run(ImageProcessor ip) {
        int [] H = ip.getHistogram();
        //Histogramm kann jetzt verwendet werden
    }

    void showAbout() {
        IJ.showMessage("Test");
    }

}
